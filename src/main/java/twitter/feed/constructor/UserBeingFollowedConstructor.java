package twitter.feed.constructor;

import twitter.feed.domain.User;
import twitter.feed.util.RegexToSplitOn;

import java.util.HashSet;
import java.util.Set;

/**
 * Constructor class which maps the users being followed by a specific user
 */
public class UserBeingFollowedConstructor {

    /**
     * Traverses the user's being followed by a specific user.
     */
    public void createFollowedUsers(final User currentUser, final Set<User> users, final String followedUserString) {
        final Set<User> followedUsers = new HashSet();
        final String[] followedUsersArrString = followedUserString.split(RegexToSplitOn.splitSting(2));
        if (followedUsersArrString != null && followedUsersArrString.length > 0) {
            for (final String followedUser : followedUsersArrString) {
                final User user = new User(followedUser);
                currentUser.getFollowedUsers().add(user);
                followedUsers.add(user);
            }
        }
        updateFollowedUsersOfExistingUser(currentUser, users);
        updateUsersWithUnknownFollowedUsers(users, followedUsers);
    }

    /**
     * Adds users to the list of followers.
     */
    private void updateFollowedUsersOfExistingUser(final User currentUser, final Set<User> users) {
        for (final User user : users) {
            if (user.equals(currentUser)) {
                user.getFollowedUsers().addAll(currentUser.getFollowedUsers());
            }
        }
    }

    /**
     * Adds user to the set of all users if the user is not in the set already.
     */
    private void updateUsersWithUnknownFollowedUsers(final Set<User> users, final Set<User> followedUsers) {
        for (final User followedUser: followedUsers) {
            if (!users.contains(followedUser)) {
                users.add(followedUser);
            }
        }
    }
}
