package twitter.feed.constructor;

import twitter.feed.domain.User;
import twitter.feed.util.FileReader;
import twitter.feed.util.RegexToSplitOn;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class which creates the set of users from the file
 */
public class UsersConstructor {

    private UserBeingFollowedConstructor userBeingFollowedConstructor;

    public UsersConstructor() {
        userBeingFollowedConstructor = new UserBeingFollowedConstructor();
    }

    /**
     * Traverses the users file and creates the users set.
     * @throws FileNotFoundException exception thrown when the user file cannot be found.
     */
    public Set<User> createUsers(final String userFile) throws FileNotFoundException {
        final Set<User> users = new HashSet();
        final List<String> usersFromFile = FileReader.getInstance().readFile(userFile);
        if (usersFromFile != null && !usersFromFile.isEmpty()) {
            for (final String userFromFile : usersFromFile) {
                final String[] userArrString = userFromFile.split(RegexToSplitOn.splitSting(1));
                if (userArrString != null && userArrString.length > 0) {
                    addUsers(users, userArrString);
                }
            }
        }
        return users;
    }

    /**
     * Adds the user to the set and also calls the user following constructor.
     */
    private void addUsers(final Set<User> users, final String[] userString) {
        final User user = new User(userString[0]);
        if (userString.length > 1) {
            userBeingFollowedConstructor.createFollowedUsers(user, users, userString[1]);
        }
        users.add(user);
    }
}
