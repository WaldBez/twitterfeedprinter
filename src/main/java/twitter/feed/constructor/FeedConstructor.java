package twitter.feed.constructor;

import twitter.feed.domain.User;
import twitter.feed.exception.TweetToLargeException;
import twitter.feed.util.FileReader;
import twitter.feed.util.RegexToSplitOn;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Set;

/**
 * Constructs the list of tweets for each user.
 */
public class FeedConstructor {

    /**
     * Traverses the tweets and maps them back to the user.
     * @throws FileNotFoundException exception thrown when the tweet file cannot be found.
     */
    public void createTweets(final Set<User> users, final String tweetFile) throws FileNotFoundException, TweetToLargeException {
        final List<String> tweetsFromFile = FileReader.getInstance().readFile(tweetFile);
        if (tweetsFromFile != null && !tweetsFromFile.isEmpty()) {
            for (final String tweetFromFile : tweetsFromFile) {
                final String[] tweetArrString = tweetFromFile.split(RegexToSplitOn.splitSting(3));
                if (tweetArrString != null && tweetArrString.length > 0) {
                    addTweetToUsers(users, tweetArrString);
                }
            }
        }
    }

    /**
     * Adds the tweets to a specific user.
     */
    private void addTweetToUsers(final Set<User> users, final String[] tweetFromFile) throws TweetToLargeException {
        for (final User user : users) {
            if (user.equals(new User(tweetFromFile[0]))) {
                if (tweetFromFile[1].length() > 140) {
                    throw new TweetToLargeException(tweetFromFile[1]);
                } else {
                    user.getTweets().add(tweetFromFile[1]);
                }
            }
        }
    }
}
