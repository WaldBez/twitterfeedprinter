package twitter.feed.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User object which contains the user's name, the users being
 * followed and the tweets the user posted
 */
public class User {

    private String userName;
    private Set<User> followedUsers;
    private List<String> tweets;

    public User(final String userName) {
        this.userName = userName;
        this.followedUsers = new HashSet();
        this.tweets = new ArrayList();
    }

    /**
     * Overriden equesls method to ensure that unique users are determined by there user name
     */
    @Override
    public boolean equals(final Object otherUser) {
        return userName != null ?
               userName.equals(((User)otherUser).userName) :
               ((User)otherUser).userName == null;
    }

    /**
     * New hashCode which is now based on the user name
     */
    @Override
    public int hashCode() {
        return userName != null ?
               userName.hashCode() : 0;
    }

    public String getUserName() {
        return userName;
    }

    public Set<User> getFollowedUsers() {
        return followedUsers;
    }

    public List<String> getTweets() {
        return tweets;
    }
}
