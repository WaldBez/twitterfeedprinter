package twitter.feed;

import twitter.feed.constructor.FeedConstructor;
import twitter.feed.constructor.UsersConstructor;
import twitter.feed.domain.User;
import twitter.feed.exception.NoUserException;
import twitter.feed.exception.TweetToLargeException;
import twitter.feed.util.TweetPrinter;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

public class Main {

    final static UsersConstructor usersConstructor = new UsersConstructor();
    final static FeedConstructor feedConstructor = new FeedConstructor();

    public static void main(String[] args) {
        if (args.length == 2) {
            final Set<User> userSet = new HashSet<User>();

            try {
                userSet.addAll(usersConstructor.createUsers(args[0]));
            } catch (FileNotFoundException e) {
                System.out.println("User file not found.");
                e.printStackTrace();
            }

            try {
                feedConstructor.createTweets(userSet, args[1]);
            } catch (TweetToLargeException e) {
                System.out.println(e.toString());
            } catch (FileNotFoundException e) {
                System.out.println("Tweet file not found.");
                e.printStackTrace();
            }

            try {
                TweetPrinter.printUserFeeds(userSet);
            } catch (NoUserException e) {
                System.out.println(e.toString());
            }
        } else {
            System.out.println("Not the correct number of arguments passed. Please ensure that 2 arguments are passed in.");
        }
    }
}
