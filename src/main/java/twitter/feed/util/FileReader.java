package twitter.feed.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Singleton File Reader.
 */
public class FileReader {

    private static FileReader reader = null;

    private FileReader(){}

    /**
     * Instantiates the Single instance of the file reader if it does not exist
     * else it returns the existing file reader.
     */
    public static FileReader getInstance() {
        if (reader == null) {
            reader = new FileReader();
        }
        return reader;
    }

    /**
     * Reads the file from the provided location.
     * @throws FileNotFoundException is thrown when the file cannot be found at the providerd file path.
     */
    public List<String> readFile(final String filePath) throws FileNotFoundException {
        final File file = new File(filePath);
        final Scanner scanner = new Scanner(file);
        return readFromScanner(scanner);
    }

    /**
     * Scans the data in the file and returns it as a list.
     */
    private List<String> readFromScanner(final Scanner scanner) {
        final List<String> fileLines = new ArrayList();
        while (scanner.hasNextLine()) {
            fileLines.add(scanner.nextLine());
        }
        scanner.close();
        return fileLines;
    }
}
