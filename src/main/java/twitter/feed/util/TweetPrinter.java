package twitter.feed.util;

import twitter.feed.domain.User;
import twitter.feed.exception.NoUserException;

import java.util.Set;

/**
 * Prints the user twitter feeds
 */
public class TweetPrinter {

    /**
     * Prints the user name
     */
    public static void printUserFeeds(final Set<User> users) throws NoUserException {
        if (users != null) {
            for (final User user : users) {
                System.out.println(user.getUserName());
                printTweet(user);
            }
        } else {
            throw new NoUserException(users);
        }
    }

    /**
     * Prints the user's tweets
     */
    private static void printTweet(final User user) {
        final String userName = user.getUserName();
        for (final String tweet : user.getTweets()) {
            System.out.println(String.format("\t @%s:%s", userName, tweet));
        }
    }
}
