package twitter.feed.util;

/**
 * Class to determine which regular expression to split on
 */
public class RegexToSplitOn {

    public static final int USER = 1;
    public static final int FOLLOWERS = 2;
    public static final int TWEET = 3;

    /**
     * Determines which regular expression to split a string from
     */
    public static String splitSting(final int stringType) {
        switch (stringType) {
            case USER:
                return " follows ";
            case FOLLOWERS:
                return ", ";
            case TWEET:
                return ">";
            default:
                return "Not a valid stringType";
        }
    }
}
