package twitter.feed.exception;

public class TweetToLargeException extends Exception {

    private String tweet;

    public TweetToLargeException(final String tweet) {
        this.tweet = tweet;
    }

    @Override
    public String toString() {
        return "Tweet is larger than 140 characters: " + tweet;
    }
}