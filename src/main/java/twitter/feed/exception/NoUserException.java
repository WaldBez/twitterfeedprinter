package twitter.feed.exception;

import twitter.feed.domain.User;

import java.util.Set;

/**
 * Custom exception which is thrown when no users exist.
 */
public class NoUserException extends Exception {

    private Set<User> users;

    public NoUserException(final Set<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "No user found in the set of users " + users;
    }
}
