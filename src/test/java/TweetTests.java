import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import twitter.feed.constructor.FeedConstructor;
import twitter.feed.domain.User;
import twitter.feed.exception.TweetToLargeException;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class TweetTests {

    private Set<User> users;


    @BeforeEach
    public void init() {
        users = new HashSet<User>();
        final Set<User> usersPeterFollow = new HashSet();
        usersPeterFollow.add(new User("John"));
        final Set<User> usersJohnFollow = new HashSet();
        usersJohnFollow.add(new User("Peter"));
        users.add(createUser("Peter", usersPeterFollow));
        users.add(createUser("John", usersJohnFollow));
    }

    @Test
    public void allUsersInSetTest() throws FileNotFoundException {
        final FeedConstructor feedConstructor = new FeedConstructor();
        try {
            feedConstructor.createTweets(users, this.getClass().getResource("testLongTweet.txt").getPath());
            fail("Excepotion Not Thrown");
        } catch (TweetToLargeException e) {
            assertTrue(true);
        }
    }

    @Test
    public void johnTweetedTwice() throws FileNotFoundException, TweetToLargeException {
        final FeedConstructor feedConstructor = new FeedConstructor();
        feedConstructor.createTweets(users, this.getClass().getResource("testTweets.txt").getPath());
        for (final User user : users) {
            if (user.equals(new User("John"))) {
                assertTrue(user.getTweets().size() == 2);
            }
        }
    }

    private User createUser(final String userName, final Set<User> followedUsers) {
        final User user = new User(userName);
        user.getFollowedUsers().addAll(followedUsers);
        return user;
    }
}
