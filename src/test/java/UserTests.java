import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import twitter.feed.constructor.UsersConstructor;
import twitter.feed.domain.User;
import java.io.FileNotFoundException;
import java.util.Set;

public class UserTests {

    @Test
    public void allUsersInSetTest() throws FileNotFoundException {
        final UsersConstructor usersConstructor = new UsersConstructor();
        final Set<User> users = usersConstructor.createUsers(this.getClass().getResource("testUser.txt").getPath());
        assertEquals(3, users.size());
    }

    @Test
    public void setContainsUserWithNamePeterTest() throws FileNotFoundException {
        final UsersConstructor usersConstructor = new UsersConstructor();
        final Set<User> users = usersConstructor.createUsers(this.getClass().getResource("testUser.txt").getPath());
        assertTrue(users.contains(new User("Peter")));
    }

    @Test
    public void johnFollowsTwoUsersTest() throws FileNotFoundException {
        final UsersConstructor usersConstructor = new UsersConstructor();
        final Set<User> users = usersConstructor.createUsers(this.getClass().getResource("testUser.txt").getPath());
        for (final User user : users) {
            if (user.equals(new User("John"))) {
                assertEquals(2, user.getFollowedUsers().size());
            }
        }
    }

    @Test
    public void johnFollowsUserWithNameEricTest() throws FileNotFoundException {
        final UsersConstructor usersConstructor = new UsersConstructor();
        final Set<User> users = usersConstructor.createUsers(this.getClass().getResource("testUser.txt").getPath());
        for (final User user : users) {
            if (user.equals(new User("John"))) {
                assertTrue(user.getFollowedUsers().contains(new User("Eric")));
            }
        }
    }
}
